//
//  SomeObjectivecThing.h
//  Interoping
//
//  Created by James Cash on 03-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SomeObjectivecThing : NSObject

+ (NSInteger)foo;

+ (NSString * _Nonnull)yellingString:(NSString* _Nullable)arg;

@end
