//
//  SomeObjectivecThing.m
//  Interoping
//
//  Created by James Cash on 03-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "SomeObjectivecThing.h"

@implementation SomeObjectivecThing

+ (NSInteger)foo
{
    return 5;
}

+ (NSString * _Nonnull)yellingString:(NSString* _Nullable)arg
{
    if (arg == nil) {
        return @"LOUD NOISES!";
    }
    return [[arg uppercaseString] stringByAppendingString:@"!"];
}

@end
