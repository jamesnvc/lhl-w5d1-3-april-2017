//
//  SwiftyThing.swift
//  Interoping
//
//  Created by James Cash on 03-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import Foundation

@objc class MyThing : NSObject {
    let viewController: ViewController? = nil

    func doThing() {
        SomeObjectivecThing.foo()
    }
}
