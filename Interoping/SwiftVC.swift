//
//  SwiftVC.swift
//  Interoping
//
//  Created by James Cash on 03-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit

class SwiftVC: UIViewController {
    var foo: String! = nil

    override func viewDidLoad() {
        foo = "thing"
    }
}
