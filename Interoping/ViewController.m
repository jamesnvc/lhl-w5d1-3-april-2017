//
//  ViewController.m
//  Interoping
//
//  Created by James Cash on 03-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "Interoping-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[[MyThing alloc] init] doThing];
    Foo *f = [[Foo alloc] init];
    Bar *b = [[Bar alloc] init];

    NSDate *foo = [[NSDate alloc] init];
    foo = nil;
//    foo = @"fo";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
